class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: prepare_hotspots
baseCommand:
  - tvcutils
  - prepare_hotspots
inputs:
  - id: reference
    type: File
    inputBinding:
      position: 0
      prefix: '--reference'
    label: Reference genome
    doc: FASTA file containing reference genome
    secondaryFiles:
      - .fai
      - .tmap.anno
      - .tmap.bwt
      - .tmap.pac
      - .tmap.sa
  - id: hotspots_bed
    type: File
    inputBinding:
      position: 1
      prefix: '--input-bed'
  - id: targets_unmerged_bed
    type: File
    inputBinding:
      position: 1
      prefix: '--unmerged-bed'
outputs:
  - id: hotspots_vcf
    type: File
    outputBinding:
      glob: hotspots_left_aligned.vcf
  - id: hotspots_left_aligned_bed
    type: File
    outputBinding:
      glob: hotspots_left_aligned.bed
arguments:
  - position: 0
    prefix: '--output-bed'
    valueFrom: hotspots_left_aligned.bed
  - position: 0
    prefix: '--output-vcf'
    valueFrom: hotspots_left_aligned.vcf
  - position: 0
    prefix: '--left-alignment'
    valueFrom: on
  - position: 0
    prefix: '--allow-block-substitutions'
    valueFrom: on
hints:
  - class: DockerRequirement
    dockerPull: sgsfak/tmap-tvc

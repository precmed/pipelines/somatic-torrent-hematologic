class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: prepare_bed
baseCommand:
  - tvcutils
  - validate_bed
inputs:
  - id: reference
    type: File
    inputBinding:
      position: 0
      prefix: '--reference'
    label: Reference genome
    doc: FASTA file containing reference genome
    secondaryFiles:
      - .fai
      - .tmap.anno
      - .tmap.bwt
      - .tmap.pac
      - .tmap.sa
  - id: designed_bed
    type: File
    inputBinding:
      position: 1
      prefix: '--target-regions-bed'
outputs:
  - id: merged_plain_bed
    doc: A BED to be used as input to --region-bed argument of variant_caller_pipeline.py 
    type: File
    outputBinding:
      glob: merged_plain.bed
  - id: unmerged_detail_bed
    doc: An unmerged BED to be used as input to --primer-trim-bed argument of variant_caller_pipeline.py
    type: File
    outputBinding:
      glob: unmerged_detail.bed
arguments:
  - position: 0
    prefix: '--unmerged-detail-bed'
    valueFrom: unmerged_detail.bed
  - position: 0
    prefix: '--merged-plain-bed'
    valueFrom: merged_plain.bed
hints:
  - class: DockerRequirement
    dockerPull: sgsfak/tmap-tvc
